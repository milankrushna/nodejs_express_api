var express = require('express');
var router = express.Router();
//for more details visit : https://express-validator.github.io/docs/index.html
const { body,check,param } = require('express-validator');
var documentController = require("../controllers/documentsController")

router.post("/",
[
    body('email').custom((value, { req }) => {
       
       console.log(value);
       
        if(req.body.phone == "" && value == ""){
           
              throw new Error('Email id is required');
            
        }

        if(value !== ""){
        var parten = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/
        if(!parten.test(value))  throw new Error('Please input a valid email');
        }
        // Indicates the success of this synchronous custom validator
        return true;
      }),
    body('phone').custom((value, { req }) => {
       
        if(req.body.email == "" && value == ""){
            throw new Error('Phone number is required');
        }

        if(value !== ""){
            console.log(value.length);
            
        if( !/^\d+$/.test(value) || value.length != "10")  throw new Error('Please input a valid Phone number');
        }

        // Indicates the success of this synchronous custom validator
        return true;
      }),
],documentController.createDocument);



///get the document Information

router.get("/:documentId",documentController.checkDocumentStatus)
router.put("/updateDocument",
[
    body("document_id").notEmpty().withMessage("Document id required")
],documentController.updateDocument)
router.delete("/:documentId",documentController.deletDocument)

module.exports = router