var environment = require("../cred/env")
const { validationResult } = require('express-validator')
const Document = require("../models/documentsModel")

exports.createDocument = (req,res,next)=>{
    
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({status:0,message:"Something error!", errors: errors.array() });
    }
var data = req.body

    Document.insertDocument(data,response=>{
        res.status(200).json(response);
    })

}

exports.checkDocumentStatus = (req,res,next)=>{
    

//...your code goes here
res.json({get:req.params})
}
exports.deletDocument = (req,res,next)=>{
    

//...your code goes here
res.json({delete:req.params})
}

exports.updateDocument = (req,res,next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({status:0,message:"Something error!", errors: errors.array() });
    } 

//...your code goes here
res.json({update:req.body})
}