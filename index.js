const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const environment = require('./cred/env');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

app.use((err, req, res, next) => {

    if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        console.error(err);
        return res.status(400).json({status:0,message:"put a valid request body"}); // Bad request
    }

    // next();
});

app.use('/document', require("./routes/documents"));


//404 request handeller
app.use((req,res,next)=>{

    res.status("400").json({
        status : 0,
        message : "404 No routes found"
    })

})


app.listen(environment.port, () => console.log(`Example app listening at http://localhost:${environment.port}`))